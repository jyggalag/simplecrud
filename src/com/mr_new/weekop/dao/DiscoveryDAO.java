package com.mr_new.weekop.dao;

import java.util.List;

import com.mr_new.weekop.model.Discovery;

public interface DiscoveryDAO extends GenericDAO<Discovery, Long> {
	List<Discovery> getAll();
}
