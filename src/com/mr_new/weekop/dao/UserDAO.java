package com.mr_new.weekop.dao;

import java.util.List;

import com.mr_new.weekop.model.User;

public interface UserDAO extends GenericDAO<User, Long> {
	List<User> getAll();
	User getUserByUsername(String username);
}
