package com.mr_new.weekop.dao;

import com.mr_new.weekop.model.Vote;

public interface VoteDAO extends GenericDAO<Vote, Long>{
	public Vote getVoteByUserIdDiscoveryId(long userId, long discoveryId);
}
