package com.mr_new.weekop.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDAO<T, PK extends Serializable> {
	// CRUD
	T create(T discovery);
	T read(PK primaryKey);
	boolean update(T discovery);
	boolean delete(PK key);
	List<T> getAll();
}
